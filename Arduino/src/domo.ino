/*============================================================================*\
||              ___                     _____
||             /   \___  _ __ ___   ___/__   \_ __ ___  _ __
||            / /\ / _ \| '_ ` _ \ / _ \ / /\/ '__/ _ \| '_ \
||           / /_// (_) | | | | | | (_) / /  | | | (_) | | | |
||          /___,' \___/|_| |_| |_|\___/\/   |_|  \___/|_| |_|
||
\*============================================================================*/

// TODO: Switch to a faster json lib
// TODO: Add links to libs

// Libs
#include <Arduino_FreeRTOS.h>
#include <croutine.h>
#include <event_groups.h>
#include <FreeRTOSConfig.h>
#include <FreeRTOSVariant.h>
#include <list.h>
#include <mpu_wrappers.h>
#include <portable.h>
#include <portmacro.h>
#include <projdefs.h>
#include <queue.h>
#include <semphr.h>
#include <StackMacros.h>
#include <task.h>
#include <timers.h>
#include <Key.h>
#include <Keypad.h>
#include <SPI.h>
#include <MFRC522.h>
#include <LiquidCrystal.h>
#include <idDHTLib.h>
#include <Servo.h>
#include <ArduinoJson.h>

// Pins
// TODO: Refactor some names
#define LED_GAR 36
#define LDR_PIN A0

#define LED 47
#define MTR_PIN 9
#define GBT_PIN 8

#define LED_ROOM2 37
#define LED_ROOM3 38
#define LED_ROOM4 39
#define LED_ROOM5 40
#define LED_ROOM6 41
#define LED_ROOM7 42
#define LED_ROOM8 43
#define LED_ROOM1 44

#define MAG_PIN 10
#define BUZ_PIN 7
#define PIR_PIN 46
#define FAN_PIN 6

#define DHT_SENSOR_TYPE DHT_TYPE_22
#define DHT_PIN 3
#define DHT_INT 1

#define SS_PIN 53
#define RST_PIN PC6
#define IRQ_PIN 2

const byte ROWS = 4;
const byte COLS = 4;
byte colPins[ROWS] = {35,34,33,32};
byte rowPins[COLS] = {31,30,29,28};

LiquidCrystal lcd(22, 23, 24, 25, 26, 27);
MFRC522 mfrc522(SS_PIN, RST_PIN);

//  Global Vars
// TODO: Merge vars into an app state struct/object
// TODO: Refactor some names
volatile uint8_t pir_value = 0;
volatile uint8_t ldr_value = 0;
volatile uint8_t mag_value = 0;
volatile uint8_t lms_value = 0;

volatile float dht_h_value = 0;
volatile float dht_t_value = 0;

volatile boolean mtr_state = false;
volatile boolean global_state = false;
volatile boolean alarm_state = false;

volatile char inChar;
volatile boolean stringComplete = false;
String inputString = "";

char password[4] = {'A','B','C','D'};
char attempt[4] = {0,0,0,0};
uint8_t keyCount = 0;
char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

volatile boolean bNewInt = false;
unsigned char regVal = 0x7F;

void activateRec(MFRC522 mfrc522);
void clearInt(MFRC522 mfrc522);

int codice [4]; // pw
int(codicevalido [4]) = {164, 87, 154, 187};
int(codicemaster [4]) = {227,121,53,2};

Keypad tastierino = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );
MFRC522::MIFARE_Key key;
Servo myservo;


// Func Prototypes
void dhtLib_wrapper();
void ledTask( void *p );
void fanTask( void *p );
void lmsTask( void *p );
void magTask( void *p );
void pirTask( void *p );
void buzTask( void *p );
void ldrTask( void *p );
void dhtTask( void *p );
void keyTask( void *p );
void nfcTask( void *p );
void lcdTask( void *p );
void logTask( void *p );
void srxTask( void *p );

//  sys func handler for task intercom
TaskHandle_t buzh;
TaskHandle_t keyh;

// sem mutex
SemaphoreHandle_t pirMutex;
SemaphoreHandle_t mtrMutex;
SemaphoreHandle_t magMutex;
SemaphoreHandle_t ldrMutex;
SemaphoreHandle_t dhtMutex;
SemaphoreHandle_t nfcMutex;
SemaphoreHandle_t alarmMutex;
SemaphoreHandle_t serialMutex;
SemaphoreHandle_t globalMutex;

idDHTLib DHTLib(DHT_PIN, DHT_INT, dhtLib_wrapper); // this lib must init before setup func

void setup() {
  SPI.begin(); // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522 card
  myservo.attach(MTR_PIN);
  myservo.write(0);
  inputString.reserve(64);
  lcd.begin(16, 2);
  lcd.clear();
  lcd.print("SmartHouse");

  Serial.begin( 115200 );
  while(!Serial){}
  Serial.println(F("Serial ok"));
  delay(1000);


  Serial.println(F("Mutex init"));

  if ( serialMutex == NULL ){
    serialMutex = xSemaphoreCreateMutex();
    if ( ( serialMutex ) != NULL ){ xSemaphoreGive( ( serialMutex ) ); }
  }

  if ( mtrMutex == NULL ){
    mtrMutex = xSemaphoreCreateMutex();
    if ( ( mtrMutex ) != NULL ){ xSemaphoreGive( ( mtrMutex ) ); }
  }


  if ( ldrMutex == NULL ){
    ldrMutex = xSemaphoreCreateMutex();
    if ( ( ldrMutex ) != NULL ) { xSemaphoreGive( ( ldrMutex ) ); }
  }


  if ( nfcMutex == NULL ){
    nfcMutex = xSemaphoreCreateMutex();
    if ( ( nfcMutex ) != NULL ) { xSemaphoreGive( ( nfcMutex ) ); }
  }


  if ( dhtMutex == NULL ){
    dhtMutex = xSemaphoreCreateMutex();
    if ( ( dhtMutex ) != NULL ) { xSemaphoreGive( ( dhtMutex ) ); }
  }

  if ( magMutex == NULL ){
    magMutex = xSemaphoreCreateMutex();
    if ( ( magMutex ) != NULL ) { xSemaphoreGive( ( magMutex ) ); }
  }

  if ( pirMutex == NULL ){
    pirMutex = xSemaphoreCreateMutex();
    if ( ( pirMutex ) != NULL ) { xSemaphoreGive( ( pirMutex ) ); }
  }

  if ( globalMutex == NULL ){
    globalMutex = xSemaphoreCreateMutex();
    if ( ( globalMutex ) != NULL ) { xSemaphoreGive( ( globalMutex ) ); }
  }

  if ( alarmMutex == NULL ){
    alarmMutex = xSemaphoreCreateMutex();
    if ( ( alarmMutex ) != NULL ) { xSemaphoreGive( ( alarmMutex ) ); }
  }
  Serial.println(F("Mutex ok"));
  delay(1000);

  Serial.println(F("Sys process init"));

  xTaskCreate(ledTask, (const portCHAR *)"LED Task", 128, NULL, 1, NULL);

  xTaskCreate(lcdTask, (const portCHAR *)"LCD Task", 256, NULL, 1, NULL);

  xTaskCreate(ldrTask, (const portCHAR *)"LDR Task", 128, NULL, 1, NULL);

  xTaskCreate(fanTask, (const portCHAR *)"FAN Task", 128, NULL, 1, NULL);

  xTaskCreate(nfcTask, (const portCHAR *)"RFID Task", 1024, NULL, 1, NULL);

  xTaskCreate(dhtTask, (const portCHAR *)"DHT Task", 256, NULL, 1, NULL);

  xTaskCreate(mtrTask, (const portCHAR *)"Motor Task", 128, NULL, 1, NULL);

  xTaskCreate(keyTask, (const portCHAR *)"KEY Task", 128, NULL, 1, &keyh);

  xTaskCreate(pirTask, (const portCHAR *)"PIR Task", 128, NULL, 1, NULL);

  xTaskCreate(magTask, (const portCHAR *)"REED Task", 128, NULL, 1, NULL);

  xTaskCreate(buzTask, (const portCHAR *)"Alarm check cycle", 128, NULL, 1, &buzh);

  xTaskCreate(logTask, (const portCHAR *)"LOG Task", 512, NULL, 1, NULL);

  xTaskCreate(srxTask, (const portCHAR *)"Server communication Task", 512, NULL, 1, NULL);


  Serial.println(F("Scheduler init "));

  for(int i = 0; i<15; i++){
    delay(200);
    Serial.print(".");
  }

  delay(1000);
  Serial.println(F(""));

  delay(1000);
  Serial.println(F("Scheduler started"));

  vTaskStartScheduler();

  Serial.println(F("Wild Missingno appeared!"));
  Serial.print(F("Software is confused"));
  Serial.println(F("Software fainted"));
  while(1);
}

void loop(){}

// TODO: Think about cooperative scheduler for some tasks

void srxTask(void *p){
  // TODO: Absolute need to refactor this
  uint8_t c;

  pinMode(LED_ROOM1, OUTPUT);
  pinMode(LED_ROOM2, OUTPUT);
  pinMode(LED_ROOM3, OUTPUT);
  pinMode(LED_ROOM4, OUTPUT);
  pinMode(LED_ROOM5, OUTPUT);
  pinMode(LED_ROOM6, OUTPUT);
  pinMode(LED_ROOM7, OUTPUT);
  pinMode(LED_ROOM8, OUTPUT);

  for (;;){

    if(Serial.available() > 0) {
      if ( xSemaphoreTake( serialMutex, ( TickType_t ) 5 ) == pdTRUE ){
        c = Serial.read();// read the incoming data as string
        xSemaphoreGive( serialMutex );
      }

      switch(c){
        case 'a':
          digitalWrite(LED_ROOM1,HIGH);
          break;
        case 'A':
          digitalWrite(LED_ROOM1,LOW);
          break;
        case 'b':
          digitalWrite(LED_ROOM2,HIGH);
          break;
        case 'B':
          digitalWrite(LED_ROOM2,LOW);
          break;
        case 'c':
          digitalWrite(LED_ROOM3,HIGH);
          break;
        case 'C':
          digitalWrite(LED_ROOM3,LOW);
          break;
        case 'd':
          digitalWrite(LED_ROOM4,HIGH);
          break;
        case 'D':
          digitalWrite(LED_ROOM4,LOW);
          break;
        case 'e':
          digitalWrite(LED_ROOM5,HIGH);
          break;
        case 'E':
          digitalWrite(LED_ROOM5,LOW);
          break;
        case 'f':
          digitalWrite(LED_ROOM6,HIGH);
          break;
        case 'F':
          digitalWrite(LED_ROOM6,LOW);
          break;
        case 'g':
          digitalWrite(LED_ROOM7,HIGH);
          break;
        case 'G':
          digitalWrite(LED_ROOM7,LOW);
          break;
        case 'h':
          digitalWrite(LED_ROOM8,HIGH);
          break;
        case 'H':
          digitalWrite(LED_ROOM8,LOW);
        case 'i':
          myservo.write(0);
          break;
        case 'I':
          myservo.write(90);
          break;
        default:
          ;
      }
    }
    vTaskDelay((250L * configTICK_RATE_HZ) / 1000L);

  }

}

void ldrTask(void *p){

  static uint8_t val;

  for (;;){

    val = analogRead(LDR_PIN);

    if ( xSemaphoreTake( ldrMutex, ( TickType_t ) 5 ) == pdTRUE ){
      ldr_value = val;
      xSemaphoreGive( ldrMutex );
    }

    vTaskDelay((1000L * configTICK_RATE_HZ) / 1000L);

  }

}

void lcdTask(void *p){

  static uint8_t inner_state;

  for(;;){
    if ( xSemaphoreTake( globalMutex, ( TickType_t ) 5 ) == pdTRUE ){
      inner_state = global_state;
      xSemaphoreGive( globalMutex );
    }

    if(global_state){
      lcd.setCursor(0,0);
      lcd.print("SmartHouse ON ");
    }

    else{
      lcd.setCursor(0,0);
      lcd.print("SmartHouse OFF");
    }

    vTaskDelay((250L * configTICK_RATE_HZ) / 1000L);
  }


}


void fanTask(void *p){

  static float tmp;
  static float hum;
  pinMode(FAN_PIN, OUTPUT);

  for (;;){
    if ( xSemaphoreTake( dhtMutex, ( TickType_t ) 5 ) == pdTRUE ){
      hum = dht_h_value;
      tmp = dht_t_value;
      xSemaphoreGive( dhtMutex );
    }

    (hum > 50.0 && tmp > 25.0) ? digitalWrite(FAN_PIN,HIGH) : digitalWrite(FAN_PIN,LOW);

    vTaskDelay((1000L * configTICK_RATE_HZ) / 1000L);

  }

}


void nfcTask(void *p){

  static uint8_t semaforo;
  static uint8_t inner_state;
  static uint8_t i;
  static int c = 0;


  pinMode(IRQ_PIN, INPUT_PULLUP);

  regVal = 0xA0; //rx irq
  mfrc522.PCD_WriteRegister(mfrc522.ComIEnReg,regVal);

  bNewInt = false; //interrupt flag

  // Activate interrupt
  attachInterrupt(digitalPinToInterrupt(IRQ_PIN), readCard, FALLING);


  for (;;){

    // TODO: Refactor some of this too
    if ( xSemaphoreTake( globalMutex, ( TickType_t ) 5 ) == pdTRUE ){
      inner_state = global_state;
      xSemaphoreGive( globalMutex );
    }

    if(bNewInt){  //new read interrupt
      bNewInt = false;
      mfrc522.PICC_ReadCardSerial(); //read the tag data
      // Show some details of the PICC (that is: the tag/card)

      for (i = 0; i < mfrc522.uid.size; i++){
       codice [i] = mfrc522.uid.uidByte[i]; // memorize the incoming char
      }

      semaforo = 1;
      for (i = 0; i < 4; i++){
        if ( !(codice [i] == codicevalido [i]) )
          semaforo = 0;
      }

      if (semaforo == 1){

       if ( xSemaphoreTake( mtrMutex, ( TickType_t ) 5 ) == pdTRUE ){
          mtr_state = true;
          xSemaphoreGive( mtrMutex );
        }

        if(c > 2){
          c = 0;
          myservo.write(0);
        }
        else{
          c++;
          if(c > 3){c=0;}
          myservo.write(180); // open door
        }
      }
      else{

        if ( xSemaphoreTake( mtrMutex, ( TickType_t ) 5 ) == pdTRUE ){
          mtr_state = false;
          xSemaphoreGive( mtrMutex );
        }
        myservo.write(0); // Alarm
        c = 0;
      }
      clearInt(mfrc522);
   }

   activateRec(mfrc522);
   lcd.setCursor(0,2);
   lcd.print(c);
   vTaskDelay((250L * configTICK_RATE_HZ) / 1000L);

  }

}

//Helper routine to dump a byte array as hex values to Serial.
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i], DEC);
    }
}
//MFRC522 interrupt serving routine
void readCard(){ bNewInt = true; }

// Send MFRC522 the needed commands to activate the reception
void activateRec(MFRC522 mfrc522){
    mfrc522.PCD_WriteRegister(mfrc522.FIFODataReg,mfrc522.PICC_CMD_REQA);
    mfrc522.PCD_WriteRegister(mfrc522.CommandReg,mfrc522.PCD_Transceive);
    mfrc522.PCD_WriteRegister(mfrc522.BitFramingReg, 0x87);
}

// Clear pending interrupt bits after interrupt serving routine
void clearInt(MFRC522 mfrc522){
   mfrc522.PCD_WriteRegister(mfrc522.ComIrqReg,0x7F);
}

void dhtTask(void *p){

  static float tmp;
  static float hum;
  static int result;
  pinMode(DHT_PIN, INPUT);

  for (;;){
    // Measure temperature and humidity. If the functions returns, then a measurement is available.
    result = DHTLib.acquireAndWait();
    switch (result){
      case IDDHTLIB_OK:
        hum = DHTLib.getHumidity();
        tmp = DHTLib.getCelsius();
        if ( xSemaphoreTake( dhtMutex, ( TickType_t ) 5 ) == pdTRUE ){
          dht_h_value = hum;
          dht_t_value = tmp;
          xSemaphoreGive( dhtMutex );
        }
        lcd.setCursor(0,1);
        lcd.print("H:");
        lcd.setCursor(2,1);
        lcd.print(hum, 1);
        lcd.setCursor(7,1);
        lcd.print("C:");
        lcd.setCursor(9,1);
        lcd.print(tmp, 1);
        break;
      case IDDHTLIB_ERROR_CHECKSUM:
        if ( xSemaphoreTake( serialMutex, ( TickType_t ) 5 ) == pdTRUE ){
          Serial.println("Checksum error");
          xSemaphoreGive( serialMutex );
        }
        break;
      case IDDHTLIB_ERROR_TIMEOUT:
        if ( xSemaphoreTake( serialMutex, ( TickType_t ) 5 ) == pdTRUE ){
          Serial.println("Time out error");
          xSemaphoreGive( serialMutex );
        }
        break;
      case IDDHTLIB_ERROR_ACQUIRING:
        if ( xSemaphoreTake( serialMutex, ( TickType_t ) 5 ) == pdTRUE ){
          Serial.println("Error Acquiring");
          xSemaphoreGive( serialMutex );
        }
        break;
      case IDDHTLIB_ERROR_DELTA:
        if ( xSemaphoreTake( serialMutex, ( TickType_t ) 5 ) == pdTRUE ){
          Serial.println("Delta time to small");
          xSemaphoreGive( serialMutex );
        }
        break;
      case IDDHTLIB_ERROR_NOTSTARTED:
        if ( xSemaphoreTake( serialMutex, ( TickType_t ) 5 ) == pdTRUE ){
          Serial.println("Not started");
          xSemaphoreGive( serialMutex );
        }
        break;
      default:
        if ( xSemaphoreTake( serialMutex, ( TickType_t ) 5 ) == pdTRUE ){
          Serial.println("Unknown error");
          xSemaphoreGive( serialMutex );
        }
        break;
  }

  vTaskDelay((2500L * configTICK_RATE_HZ) / 1000L);

  }
}

void dhtLib_wrapper() {
  DHTLib.dht22Callback(); // Change dht11Callback() for a dht22Callback() if you have a DHT22 sensor
}

void pirTask(void *p){

  static uint8_t val;
  static uint8_t inner_state;
  pinMode(PIR_PIN, INPUT_PULLUP);

  for (;;){

    if ( xSemaphoreTake( globalMutex, ( TickType_t ) 5 ) == pdTRUE ){
      inner_state = global_state;
      xSemaphoreGive( globalMutex );
    }

    if(inner_state){

      val = digitalRead(PIR_PIN);

      if ( xSemaphoreTake( pirMutex, ( TickType_t ) 5 ) == pdTRUE ){
        pir_value = val;
        xSemaphoreGive( pirMutex ); // Now free or "Give" the Serial Port for others.
      }

      if(val){
        if ( xSemaphoreTake( alarmMutex, ( TickType_t ) 5 ) == pdTRUE ){
          alarm_state = 1;
          xSemaphoreGive( alarmMutex );
        }
      }

      vTaskDelay((100L * configTICK_RATE_HZ) / 1000L);

    }

    vTaskDelay((200L * configTICK_RATE_HZ) / 1000L);

  }

}

void magTask(void *p){

  static uint8_t val;
  static uint8_t inner_state;
  pinMode(MAG_PIN, INPUT_PULLUP);

  for (;;){

    if ( xSemaphoreTake( globalMutex, ( TickType_t ) 5 ) == pdTRUE ){
      inner_state = global_state;
      xSemaphoreGive( globalMutex );
    }

    if(inner_state){
      val = digitalRead(MAG_PIN);

      if ( xSemaphoreTake( magMutex, ( TickType_t ) 5 ) == pdTRUE ){
        mag_value = val;
        xSemaphoreGive( magMutex );
      }

      if(val){
        if ( xSemaphoreTake( alarmMutex, ( TickType_t ) 5 ) == pdTRUE ){
          alarm_state = 1;
          xSemaphoreGive( alarmMutex );
        }
      }

      vTaskDelay((500L * configTICK_RATE_HZ) / 1000L);

    }

  }

}

// TODO: Refactor all those led names
void ledTask(void *p){

  pinMode(LED_GAR, OUTPUT);

  for (;;){

    if ( xSemaphoreTake( ldrMutex, ( TickType_t ) 5 ) == pdTRUE ){
      (ldr_value < 100.00) ? digitalWrite(LED_GAR,HIGH) : digitalWrite(LED_GAR,LOW);
      xSemaphoreGive( ldrMutex );
    }

    vTaskDelay((2500L * configTICK_RATE_HZ) / 1000L);

  }

}

void mtrTask(void *p){

  static boolean reading = false;
  static boolean previous = false;
  static boolean inner_state = false;

  pinMode(GBT_PIN, INPUT_PULLUP);

  for(;;){


    if ( xSemaphoreTake( globalMutex, ( TickType_t ) 5 ) == pdTRUE ){
      inner_state = global_state;
      xSemaphoreGive( globalMutex );
    }

    reading = digitalRead(GBT_PIN);

    if (inner_state == false && reading == true && previous == false) {
      if (mtr_state == true){

        if ( xSemaphoreTake( mtrMutex, ( TickType_t ) 5 ) == pdTRUE ){
          mtr_state = false;
          xSemaphoreGive( mtrMutex );
        }

        myservo.write(90);
      }
      else{

        if ( xSemaphoreTake( mtrMutex, ( TickType_t ) 5 ) == pdTRUE ){
          mtr_state = true;
          xSemaphoreGive( mtrMutex );
        }

        myservo.write(0);
      }
    }

  previous = reading;
  vTaskDelay((100L * configTICK_RATE_HZ) / 1000L);

  }

}

void keyTask(void *p){

  char key;

  for(;;){
    key = tastierino.getKey();

    if (key != NO_KEY){

      switch(key){

        case '*':
          keyCount=0;
          break;

        case '#':
          vTaskDelay((50L * configTICK_RATE_HZ) / 1000L);
          check();
          break;

        default:
          attempt[keyCount]=key;
          keyCount++;
          if(keyCount==4){keyCount=0;}

      }

    }

    vTaskDelay((20L * configTICK_RATE_HZ) / 1000L);

  }

}

void correct(){

  if ( xSemaphoreTake( globalMutex, ( TickType_t ) 5 ) == pdTRUE ){
    global_state = !global_state;
    xSemaphoreGive( globalMutex );
  }

  if ( xSemaphoreTake( alarmMutex, ( TickType_t ) 5 ) == pdTRUE ){
    alarm_state = 0;
    xSemaphoreGive( alarmMutex );
  }

  if ( xSemaphoreTake( serialMutex, ( TickType_t ) 5 ) == pdTRUE ){
    Serial.println("Password ACCETTATA");
    xSemaphoreGive( serialMutex );
  }

  keyCount = 0;

}

void incorrect(){

  if ( xSemaphoreTake( serialMutex, ( TickType_t ) 5 ) == pdTRUE ){
    Serial.println("Password RIFIUTATA");
    xSemaphoreGive( serialMutex );
  }

  keyCount = 0;

}

void check(){

  uint8_t ct=0;
  uint8_t i,z;

  for ( i = 0; i < 4 ; i++ ){
    if (attempt[i]==password[i]){ ct++; }
  }

  (ct==4) ? correct() : incorrect();

  for (z=0; z<4; z++){
    attempt[z]=0;
  }

}


void buzTask(void *p){

  static uint8_t inner_state;
  static uint8_t inner_alarm;
  int i;
  pinMode(BUZ_PIN, OUTPUT);
  pinMode(LED, OUTPUT);

  for(;;){

    if ( xSemaphoreTake( globalMutex, ( TickType_t ) 5 ) == pdTRUE ){
      inner_state = global_state;
      xSemaphoreGive( globalMutex );
    }

    if ( xSemaphoreTake( alarmMutex, ( TickType_t ) 5 ) == pdTRUE ){
      inner_alarm = alarm_state;
      xSemaphoreGive( alarmMutex );
    }

    if(inner_alarm && global_state){
      for(i = 0; i < 1024; i++){
        digitalWrite(LED, i/64);
        tone(BUZ_PIN, i + 2000);
      }
      for(i = 1023; i > 0; i--){
        digitalWrite(LED, i/64);
        tone(BUZ_PIN, i + 2000);
      }
    }

    else{
      noTone(BUZ_PIN);
      digitalWrite(LED, 0);
      vTaskDelay((10L * configTICK_RATE_HZ) / 1000L);
    }

  }

}


void logTask(void *p){

  StaticJsonBuffer<256> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();

  JsonObject& dht = root.createNestedObject("DHT");
  dht["temp"] = 0.0;
  dht["humi"] = 0.0;

  JsonObject& ldr = root.createNestedObject("LDR");
  ldr["lux"] = 0.0;

  JsonObject& pir = root.createNestedObject("PIR");
  pir["val"] = 0;

  JsonObject& mag = root.createNestedObject("MAG");
  mag["val"] = 0;

  for(;;){

    vTaskDelay((5000L * configTICK_RATE_HZ) / 1000L);

    if ( xSemaphoreTake( dhtMutex, ( TickType_t ) 5 ) == pdTRUE ){
      dht["humi"] = (double_with_n_digits(dht_h_value, 2));
      dht["temp"] = (double_with_n_digits(dht_t_value, 2));
      xSemaphoreGive( dhtMutex );
    }

    if ( xSemaphoreTake( ldrMutex, ( TickType_t ) 5 ) == pdTRUE ){
      ldr["lux"] = (double_with_n_digits(ldr_value, 2));
      xSemaphoreGive( ldrMutex );
    }

    if ( xSemaphoreTake( pirMutex, ( TickType_t ) 5 ) == pdTRUE ){
      pir["val"] = pir_value;
      xSemaphoreGive( pirMutex );
    }

    if ( xSemaphoreTake( magMutex, ( TickType_t ) 5 ) == pdTRUE ){
      mag["val"] = mag_value;
      xSemaphoreGive( magMutex );
    }

    if ( xSemaphoreTake( serialMutex, ( TickType_t ) 5 ) == pdTRUE ){
      root.printTo(Serial);
      Serial.println();
      xSemaphoreGive( serialMutex );
    }

  }

}
