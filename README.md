# Domotron #

POC of a custom smart home automation system using mostly js.

As a proof of concept for an High School most of the things written are hardcoded. 

Plans are to refactor the whole system into a proper iot pub/sub service and possibly modularize the Arduino sensors in a smarter way. 

Once I get the time to sketch up the wiring diagrm to arduino it will be uploaded.