// TODO: Refactor in es6
// TODO: Update express
// TODO: Add auth

var serial  = require('serialport');
var express = require('express');
var morgan  = require('morgan');
var http    = require('http');
var app     = express();
var router  = express.Router();
var port    = process.env.PORT || 3000;

var server = http.Server(app).listen(port);
var io      = require('socket.io').listen(server);


router.use(morgan('dev'));
router.use(express.static(__dirname + '/../client/build'));

router.get('/', function(req, res) {
    res.sendFile('index.html');
});

app.use('/', router);

var Serial = serial.SerialPort;
var PORT = "/dev/ttyACM4";
var myPort = new Serial(PORT,{
  baudRate : 115200,
  parser: serial.parsers.readline("\r\n")
});

myPort.on('open', function(){
  // Now server is connected to Arduino
  console.log('Serial Port Opend');

  var lastValue;
  io.on('connection', function (socket) {
    var state1 = false;
    var state2 = false;
    var state3 = false;
    var state4 = false;
    var state5 = false;
    var state6 = false;
    var state7 = false;
    var state0 = false;
    var garState = false;
    //Connecting to client
    console.log('Socket connected');
    socket.emit('connected');
    var lastValue;
    socket.id = socket;

    myPort.on('data', function(data){
      var actual = data;
      if(lastValue !== actual){
        if(data.length > 60){
          JSON.parse(actual);
        }
        socket.emit('msg', actual);
      }
      lastValue = actual;
      console.log(data);
    });

    socket.on('room1', function (socket) {
      console.log("led state1: "+state1);
      (!state1) ?  myPort.write("a") : myPort.write("A");
      state1 = !state1;
    });

    socket.on('room2', function (socket) {
      console.log("led state2: "+state2);
      (!state2) ?  myPort.write("b") : myPort.write("B");
      state2 = !state2;
    });

    socket.on('room3', function (socket) {
      console.log("led state3: "+state3);
      (!state3) ?  myPort.write("c") : myPort.write("C");
      state3 = !state3;
    });

    socket.on('room4', function (socket) {
      console.log("led state4: "+state4);
      (!state4) ?  myPort.write("d") : myPort.write("D");
      state4 = !state4;
    });

    socket.on('room5', function (socket) {
      console.log("led state5: "+state5);
      (!state5) ?  myPort.write("e") : myPort.write("E");
      state5 = !state5;
    });

    socket.on('room6', function (socket) {
      console.log("led state6: "+state6);
      (!state6) ?  myPort.write("f") : myPort.write("F");
      state6 = !state6;
    });

    socket.on('room7', function (socket) {
      console.log("led state7: "+state7);
      (!state7) ?  myPort.write("g") : myPort.write("G");
      state7 = !state7;
    });

    socket.on('room0', function (socket) {
      console.log("led state0: "+state0);
      (!state0) ?  myPort.write("h") : myPort.write("H");
      state0 = !state0;
    });
    socket.on('gar', function (socket){
      (garState) ?  myPort.write("i") : myPort.write("I");
      garState = !garState;
    });

    socket.on('disconnect', function() {
      console.log("SALUTI");
      socket.disconnect(true);
    });

  });
});

console.log('Serving on port ' + port);
