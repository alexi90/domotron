import {Component} from "@angular/core";
import {OnInit} from "@angular/core";
declare var io: any;

@Component({
    selector: 'home',
    templateUrl: './app/home/home.html',
    styleUrls: ['./app/home/home.css']
})
export class HomeComponent implements OnInit {
  socket: any;
  room: number;
  recived: Object = {DHT:{temp:1,humi:1},LDR:{lux:0.00},PIR:{val:0},MAG:{val:0}};

  constructor(){
    this.socket = io("http://192.168.1.8:3000/");
    this.socket.on("msg", (data) => {
      (data.length > 50) ? this.recived = JSON.parse(data) : console.log(this.recived);
    });
  }

  toggle(room) {
    this.socket.emit("room"+room);
    console.log("Toggling room: "+room);
  }
  gar() {
    this.socket.emit("gar");
  }
  ngOnInit() {

  }
}
