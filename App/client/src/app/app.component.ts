import {Component, OnInit} from "@angular/core";
import { ROUTER_DIRECTIVES } from '@angular/router';
import { provideRouter, RouterConfig } from '@angular/router';
import {AboutComponent} from "./about/components/about.components";
import {HomeComponent} from "./home/home.component";
import {Accordion} from "./accordion/accordion";

export const routes: RouterConfig = [
  { path: '', component: HomeComponent },
  { path: 'About', component: AboutComponent }
];

export const APP_ROUTER_PROVIDERS = [provideRouter(routes)];

@Component({
    selector: "app",
    templateUrl: "./app/app.html",
    styleUrls: ['./app/app.css'],
    directives: [Accordion, AboutComponent, ROUTER_DIRECTIVES]
})
export class AppComponent implements OnInit {
    ngOnInit() {
        console.log("Application component initialized ...");
    }
}
