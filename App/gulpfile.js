"use strict";

var gulp = require('gulp');
var runSequence = require('run-sequence');
var del = require('del');
var styl = require('gulp-stylus');
var tsc = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require("browser-sync").create();
var reload = browserSync.reload;
var tsProject = tsc.createProject("./tsconfig.json");
var tslint = require('gulp-tslint');
var rupture = require('rupture');
var autoprefixer = require('autoprefixer-stylus');
var image = require('gulp-image');
var gutil = require('gulp-util');
/**
 * Remove build directory.
 */
gulp.task('clean', function (){return del(['client/build']);});

//IMAGES
gulp.task('images', function () {
  gulp.src('client/src/images/**/*')
    .pipe(image({
      pngquant: true,
      optipng: false,
      zopflipng: true,
      advpng: true,
      jpegRecompress: false,
      jpegoptim: true,
      mozjpeg: true,
      gifsicle: true,
      svgo: true
    }))
    .pipe(gulp.dest('client/build'));
});

/**
 * Lint all custom TypeScript files.
 */
gulp.task('tslint', function(){
    return gulp.src("client/src/**/*.ts")
        .pipe(tslint())
        .pipe(tslint.report('prose'));
});

// Serving files for developing
gulp.task('serve', ['build:dev'], function () {
  browserSync.init({
    injectChanges: true,
    port: 3005,
    open: false,
    proxy: {
			"target":"localhost:3000"
		},
  	logPrefix: 'DomoSAMA',
  });
});

/**
 * Compile TypeScript sources and create sourcemaps in build directory.
 */
gulp.task("compile", function(){
    var tsResult = gulp.src("client/src/**/*.ts")
        .pipe(sourcemaps.init())
        .pipe(tsc(tsProject));
    return tsResult.js
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("client/build"))
				.pipe(browserSync.stream());
});


/**
 * Copy all resources that are not TypeScript files into build directory.
 */
gulp.task("resources", function(){
    return gulp.src(["client/src/**/*", "!**/*.ts", "!**/*.styl"])
        .pipe(gulp.dest("client/build"))
        .pipe(browserSync.stream());
});

// Compile stylus in css and stream to bs
gulp.task('styl', function(){
  return gulp.src("client/src/**/*.styl")
    .pipe(styl({use:[rupture(),autoprefixer()]}))
    .pipe(gulp.dest("client/build"))
    .pipe(browserSync.stream());
});

/**
 * Copy all required libraries into build directory.
 */
gulp.task("libs", function(){
    return gulp.src([
            'es6-shim/es6-shim.min.js',
            'systemjs/dist/system-polyfills.js',
            'systemjs/dist/system.src.js',
            'reflect-metadata/Reflect.js',
            'rxjs/**',
            'zone.js/dist/**',
            '@angular/**'
        ], {cwd: "node_modules/**"}) /* Glob required here. */
        .pipe(gulp.dest("client/build/lib"));
});

/**
 * Watch for changes in TypeScript, HTML and CSS files.
 */
gulp.task('watch', function () {
    gulp.watch(["client/src/**/*.ts"], ['compile']).on('change', function (e) {
        console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
        //browserSync.reload;
    });
    gulp.watch(["client/src/**/*.html", "client/src/**/*.styl", "client/src/images/**.*"], []).on('change', function (e) {
        console.log('Resource file ' + e.path + ' has been changed. Updating.');
        runSequence(['styl', 'resources', 'images']);
        //browserSync.reload;
    });
});

/**
 * Build the project.
 */
gulp.task('build:dev', function(cb) {
  gutil.log(gutil.colors.green("Developer Mode():::::::::::::"));
  runSequence('clean', ['compile', 'styl', 'resources', 'images', 'libs', 'watch'], cb);
});

gulp.task('build', function(cb) {
  console.log("Production Mode():::::::::::::");
  runSequence('clean', ['compile', 'styl', 'resources', 'images', 'libs'], cb);
});

gulp.task('default', ['serve']);
